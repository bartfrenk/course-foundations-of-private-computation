PROFILE := 1

export CONFIG := env/${ENV}/config.yaml

ifeq (${PROFILE},)
	RUN :=
else
	RUN := mprof run --output "mprof/data/$(shell date +"%Y%m%d_%H%M%S").dat"
endif

help: ##
	@echo "Foundations of Private Computation"
	@echo
	@fgrep -h "##" $(MAKEFILE_LIST) | \
	fgrep -v fgrep | sed -e 's/## */##/' | column -t -s##

init: ##
	pip install -r external/open-mined/requirements.txt
	for deps in requirements/*.txt; do \
	    pip install -r $$deps; \
	done;

.PHONY: notebook
notebook: ##
	DIR=$(shell pwd) jupyter notebook

qa: ##
qa: qa/mypy qa/test qa/lint

qa/lint: ##
	pylint src/* test/*

qa/mypy: ##
	mypy src/* test/*

qa/test: ##
	pytest test/*

mprof/data:
	mkdir -p mprof/data

##
